---
date: "2020-02-02"
description: "About Page"
title: about
tags: [ "about", "profile", "hooper" ]
draft: false
type: "profiles"
layout: "single"
profile:
  name: "Brian Hooper"
  image: "hooper.png"
  email: "hooper@knowntraveler.io"
  location: "Nashville, Tennessee"
  bio: |
    Husband, Father, and Technologist who is focused on serving the communities where we live, work, and play.
    I believe that teams of all sizes should enjoy the creative momentum of a modern and well-equipped platform.
  skills:
  - Architect
  - Engineer
  - Practitioner
  experience:
  -
    company: Amazon Web Services (AWS)
    url: https://aws.amazon.com/
    title: Enterprise Architect
    dates: APR 2019 - Present
    summary: |
      As an Enterprise Transformation Architect at AWS I work with enterprise customers providing practitioner level support and guidance through large modernization and transformation efforts that drive innovation.  
  -
    company: Gadgetry
    url: https://gadgetry.io
    title: Founder, Practitioner
    dates: JAN 2017 - Present
    summary: |
      As a Founder at Gadgetry.io I am focused on sharing practitioner level knowledge and expertise implementing Platform as a Service (PaaS) and advanced business capabilities through Open Source Technology, Cloud Based Services, and Industry Best Practices. 
  -
    company: Augeo
    url: https://augeomarketing.com/
    title: VP, Platform Engineering
    dates: SEP 2014 - APR 2019
    summary: |
      As the VP of Platform Engineering I was focused on delivering Platform as a Service (PaaS) and DevOps capabilities via Open Source Technology and Cloud Based Services which were capable of reacting to the highly-dynamic nature of a modern data driven services company.
  -
    company: Unum
    url: https://www.unum.com/
    title: Director, Release Management
    dates: NOV 2003 - AUG 2014
    summary: |
      Leadership role in Enterprise Delivery Services providing cross-functional coordination and decision making support to business and technology partners while influencing innovation and continuous improvement.
  education:
  - 
    dates: 2006 - 2008
    school: Central Michigan University
    url: https://www.cmich.edu/
    title: Graduate
    location: Mount Pleasant, MI
    degree: Master of Science
    summary: "Major: Information Resource Management"
  - 
    dates: 2001 - 2004
    school: Covenant Collenge
    url: https://www.covenant.edu/
    title: Undergraduate
    location: Lookout Mountain, GA
    degree: Bachelor of Science
    summary: "Major: Organizational Management"
  - 
    dates: 1997 - 2001
    school: University of Tennessee 
    url: https://www.utc.edu/
    title: Undergraduate
    location: Chattanooga, TN
    degree: 
    summary: "Major: Business Administration"
  - 
    dates: 1994 - 1997
    school: Hixson High School
    url: https://hhs.hcde.org/ 
    title: High School
    location: Hixson, TN
    degree: 
    summary: National Honors Society 
  certifications:
  -
    certification: AWS DevOps Engineer Professional
    url: https://www.credly.com/badges/6aa1675e-c600-4cdf-8505-ebfcff8ca6cf
    title: Amazon Web Services
    summary: Amazon Web Services (AWS)
    issued: JAN 2023
    expires: JAN 2026
    credential: "BR0KBR9KTNFQQ432"
  -
    certification: AWS Cloud Practitioner
    url: https://www.credly.com/badges/3627a262-49de-485a-b9f2-35c81cab2ab2
    title: Amazon Web Services
    summary: Amazon Web Services (AWS)
    issued: DEC 2021
    expires: JAN 2026
    credential: "J0S5JDGLHFVEQYGX"
  -
    certification: ITIL v3 Foundations
    url: https://en.wikipedia.org/wiki/ITIL
    title: IT Infrastructure Library
    summary: Loyalist Certification Services
    issued: DEC 2010
    expires: NEVER
    credential: "905087"
---
