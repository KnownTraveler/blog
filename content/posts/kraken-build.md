---
date: "2020-01-04"
description: "Kraken Project Custom PC Build"
title: "Kraken Project Custom PC Build"
slug: "kraken-build"
author:
  name: "Brian Hooper"
  email: "hooper@knowntraveler.io"
tags: [ "pc", "build", "kraken" ]
draft: false
summary: "Custom PC Build codenamed 'Kraken' covering the parts list, detailed specs and build steps."
---

## TLDR

This post covers the parts list and build steps for my custom PC build codenamed, "Kraken".

At the beginning of 2021 I set out to build a new workstation and gaming rig for my home office as I planned on retiring my previous build (Darkmatter) to be used as a home lab. For this new build I really wanted to build a **"beast of a machine"** that would meet the needs for work and play over the next several years.  Using [Novabench](https://novabench.com), I was able to benchmark the performance of this new build out-of-the-box using default configs at an overall ranking in the 99th percentile. 

* [See Benchmark Result](https://novabench.com/view/2284157)

### Resources

There are many great resources online, here are a few that I consulted regularly. I used PCPartPicker in particular to create my parts list, check compatibility, and price out components.

* [PCPartPicker](https://pcpartpicker.com/)
* [PCGAMER](https://www.pcgamer.com/)
* [CNET](https://www.cnet.com/)
* [newegg](https://www.newegg.com/)
* [Logical Increments](https://www.logicalincrements.com/)
* [Linus Tech Tips](https://linustechtips.com/main/)
* [Novabench](https://novabench.com)

In addition to the above resources, [Linus Tech Tips (YouTube)](https://www.youtube.com/user/LinusTechTips) publishes some great video reviews.

---

## Parts List

Here is a quick list of the core components that were used for this build:

|                                                   |                          |                                                                        |
|---------------------------------------------------|--------------------------|------------------------------------------------------------------------|
| ![Example image](/img/pc-build/cpu.png)           | &nbsp;&nbsp;&nbsp;&nbsp; | **CPU** - AND Ryzen 9 3950X 3.5 GHz 16-Core (32 Thread) Processor      |
| ![Example image](/img/pc-build/cpu-cooler.png)    | &nbsp;&nbsp;&nbsp;&nbsp; | **CPU Cooler** - NZXT Kraken X63 98.17 CFM Liquid CPU Cooler           |
| ![Example image](/img/pc-build/motherboard.png)   | &nbsp;&nbsp;&nbsp;&nbsp; | **Motherboard** - ASUS PRIME X570-PRO ATX AM4 Motherboard              |
| ![Example image](/img/pc-build/memory.png)        | &nbsp;&nbsp;&nbsp;&nbsp; | **Memory** - CORSAIR Vengeance LPX 64 GB (4 x 16GB) DDR4-3200 Memory   |
| ![Example image](/img/pc-build/m2.png)            | &nbsp;&nbsp;&nbsp;&nbsp; | **Storage (M.2)** - SAMSUNG 970 EVO 500 GB M.2-2280 Solid State Drive  |
| ![Example image](/img/pc-build/ssd.png)           | &nbsp;&nbsp;&nbsp;&nbsp; | **Storage (SSD)** - Samsung 860 Evo 500 GB 2.5" Solid State Drive (x2) |
| ![Example image](/img/pc-build/gpu.png)           | &nbsp;&nbsp;&nbsp;&nbsp; | **Video Card** - ASUS GeForce RTX 3070 8GB Dual OC Video Card          |
| ![Example image](/img/pc-build/case.png)          | &nbsp;&nbsp;&nbsp;&nbsp; | **Case** - NZXT H710 (White) ATX Mid Tower Case                        |
| ![Example image](/img/pc-build/power-supply.png)  | &nbsp;&nbsp;&nbsp;&nbsp; | **Power Supply** - EVGA SuperNOVA T2 850 W 80+ Titanium Certified PSU  |


---

## Detailed Specs

For more detailed specs on each component please review the [Completed Build on PCPartPicker](https://pcpartpicker.com/b/t3McCJ)

---

## Build Steps

This is quick overview of the build process.  

**NOTE**: Instructions for component parts will vary, please review the specs and installation steps for your build.

#### STEP 1 - Open up the case

The first step in my build process is to open up the case, removing all the panels to review all the channels and mounts where I will be installing components and running cables.

#### STEP 2 - Install the Power Supply Unit (PSU).

The Power Supply is the first component that I installed into the case. Since I selected a modular supply, it’s best to leave the cables out for now. I'll run them as I install each additional component.

#### STEP 3 - Prepare the Motherboard (CPU, M.2 NVMe SSD, RAM)

Next, it is time to prepare the motherboard by installing the CPU, M.2 NVMe SSD, and RAM. Since I have selected a Liquid CPU Cooler I will install it after mounting the motherboard inside the case. The M.2. NVMe SSD Drive has a slot directly on the motherboard underneath a heatshield.

#### STEP 4 - Install the Motherboard

Next, it is time to install the motherboard inside the case aligning the I/O panel and stand-offs. It is a good practice to seat the screws first and then proceed in a star pattern tighenening them a little bit at a time. Once the motherboard is in place there are several connections that will need to be made, including the power cabling. There are also case plugs and buttons that need to be connected to the motherboard to function properly such as USB ports, LEDs, Fans, and etc.

#### STEP 5 - Install the Liquid CPU Cooler

Now, it is time to install the iquid cpu cooler. With this particular build I decided to use a push/pull configuration with 3x 120mm fans pushing and 2x 144mm fans pulling air through the radiator. Every cpu cooler will need thermal paste. This silver paste is an excellent thermal conductor, allowing heat to efficiently transfer from the chip to the cooler. I'll use the custom bracket and stand-offs to install the cpu cooler. The radiator and dual-fans are installed at the front of the case. Once the cooler is installed there are several connections that will need to be made, including power cabling and connections to the motherboard.

#### STEP 6 - Install the Video Card (GPU)

Next, it is time to install the video graphics card. Modern graphics cards take up a PCIExpress (PCIe) slot which is a long, thin connector located on the rear of the motherboard, below the processor. To seat the card in that slot, I removed a metal backplate from my case. Once the GPU is firmly seated, I simply used the screws to fasten the card into place in the back panel. Once the cooler is installed there are several connections that will need to be made, including power cabling and connections to the motherboard.

#### STEP 7 - Install the 2.5" Solid State Drives (SSD)

Next, it is time to install the solid state drives. Once the SSDs are seated, I simply used the screws in the mounts to secure the drives. Once the drives are installed there are several SATA connections to the motherboard that will need to be made as well as power cabling.

#### STEP 8 - Preparing for First Boot

Next, once all the components are installed I will take some time to clean-up the cabling and also double-check all connections have been made. One of the best features of the NZXT H710 case that I selected for my build is the multiple channels for hiding and securing cables. There is plenty of room to spare and it offers a very clean look as well as easy access.

#### STEP 9 - First Boot

Next, is the moment of triumph... time to switch on the power supply and push the power button. Success! I am greeted by the ASUS Bios menu and am able to verify all the installed components and devices. (e.g. Memory, Disk, etc)

#### STEP 10 - Wrap-Up the Build

Lastly, I spent time installing some custom case fans, cable mods and organizing cable runs for a much cleaner look. Once everything checked out with the build it is time to install the operating systems, drivers, partition disks and etc.

---

## Next

Check out other posts on my blog for using a dual-boot configuration to run Windows 10 and Linux and Ansible to bootstrap your new workspace!
