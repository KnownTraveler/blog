---
date: "2018-04-20"
description: "Darkmatter Project Custom PC Build"
title: "Darkmatter Project Custom PC Build"
slug: "darkmatter-build"
author:
  name: "Brian Hooper"
  email: "hooper@knowntraveler.io"
tags: [ "pc", "build", "darkmatter" ]
draft: false
summary: "Custom PC Build codenamed 'Darkmatter' covering the parts list, detailed specs and build steps."
---

## TLDR

This post covers the parts list and build steps for my custom PC build codenamed, "Darkmatter".

A few months ago I set out to build a new workstation and gaming rig for my home office. The most time-consuming part of the build process was selecting components. I basically wanted to find a great balance between a performance and value that would meet the needs for work and play.

There are many great resources online, here are a few that I consulted regularly. I used PCPartPicker in particular to create my parts list, check compatibility, and price out components.

* [PCPartPicker](https://pcpartpicker.com/)
* [PCGAMER](https://www.pcgamer.com/)
* [CNET](https://www.cnet.com/)
* [newegg](https://www.newegg.com/)
* [Logical Increments](https://www.logicalincrements.com/)
* [Linus Tech Tips](https://linustechtips.com/main/)

In addition to the above resources, [Linus Tech Tips (YouTube)](https://www.youtube.com/user/LinusTechTips) publishes some great video reviews.

---

## Parts List

Here is a quick list of the core components that were used for this build:

|                                                   |                          |                                                                        |
|---------------------------------------------------|--------------------------|------------------------------------------------------------------------|
| ![Example image](/img/pc-build/cpu.png)           | &nbsp;&nbsp;&nbsp;&nbsp; | **CPU** - INTEL Core i7-8700K 3.7 GHz 6-Core Processor                 |
| ![Example image](/img/pc-build/cpu-cooler.png)    | &nbsp;&nbsp;&nbsp;&nbsp; | **CPU Cooler** - NZXT Kraken X62 Rev 2 98.17 CFM Liquid CPU Cooler     |
| ![Example image](/img/pc-build/motherboard.png)   | &nbsp;&nbsp;&nbsp;&nbsp; | **Motherboard** - ASUS ROG STRIX Z370-E GAMING ATX LGA1151             |
| ![Example image](/img/pc-build/memory.png)        | &nbsp;&nbsp;&nbsp;&nbsp; | **Memory** - CORSAIR Vengeance LPX 16 GB (4 x 8 GB) DDR4-3000 Memory   |
| ![Example image](/img/pc-build/m2.png)            | &nbsp;&nbsp;&nbsp;&nbsp; | **Storage (M.2)** - SAMSUNG 960 EVO 250 GB M.2-2280 Solid State Drive  |
| ![Example image](/img/pc-build/ssd.png)           | &nbsp;&nbsp;&nbsp;&nbsp; | **Storage (SSD)** - Samsung 860 Evo 250 GB 2.5" Solid State Drive (x2) |
| ![Example image](/img/pc-build/gpu.png)           | &nbsp;&nbsp;&nbsp;&nbsp; | **Video Card** - ASUS GeForce GTX 1060 6GB 6 GB Dual Video Card        |
| ![Example image](/img/pc-build/case.png)          | &nbsp;&nbsp;&nbsp;&nbsp; | **Case** - NZXT S340 (Black) ATX Mid Tower Case                        |
| ![Example image](/img/pc-build/power-supply.png)  | &nbsp;&nbsp;&nbsp;&nbsp; | **Power Supply** - EVGA SuperNOVA G3 650 W 80+ Gold Certified          |


---

## Detailed Specs

For more detailed specs on each component please review the [Completed Build on PCPartPicker](https://pcpartpicker.com/b/JL7TwP)

---

## Build Steps

This is quick overview of the build process.  

**NOTE**: Instructions for component parts will vary, please review the specs and installation steps for your build.

#### STEP 1 - Open up the case

The first step in my build process is to open up the case, removing all the panels to review all the channels and mounts where I will be installing components and running cables.

#### STEP 2 - Install the Power Supply Unit (PSU).

The Power Supply is the first component that I installed into the case. Since I selected a modular supply, it’s best to leave the cables out for now. I'll run them as I install each additional component.

#### STEP 3 - Prepare the Motherboard (CPU, M.2 NVMe SSD, RAM)

Next, it is time to prepare the motherboard by installing the CPU, M.2 NVMe SSD, and RAM. Since I have selected a Liquid CPU Cooler I will install it after mounting the motherboard inside the case. The M.2. NVMe SSD Drive has a slot directly on the motherboard underneath a heatshield.

#### STEP 4 - Install the Motherboard

Next, it is time to install the motherboard inside the case aligning the I/O panel and stand-offs. It is a good practice to seat the screws first and then proceed in a star pattern tighenening them a little bit at a time. Once the motherboard is in place there are several connections that will need to be made, including the power cabling. There are also case plugs and buttons that need to be connected to the motherboard to function properly such as USB ports, LEDs, Fans, and etc.

#### STEP 5 - Install the Liquid CPU Cooler

Next, it is time to install the iquid cpu cooler. Every cpu cooler will need thermal paste. This silver paste is an excellent thermal conductor, allowing heat to efficiently transfer from the chip to the cooler. I'll use the custom bracket and stand-offs to install the cpu cooler. The radiator and dual-fans are installed at the front of the case. Once the cooler is installed there are several connections that will need to be made, including power cabling and connections to the motherboard.

#### STEP 6 - Install the Video Card (GPU)

Next, it is time to install the video graphics card. Modern graphics cards take up a PCIExpress (PCIe) slot which is a long, thin connector located on the rear of the motherboard, below the processor. To seat the card in that slot, I removed a metal backplate from my case. Once the GPU is firmly seated, I simply used the screws to fasten the card into place in the back panel. Once the cooler is installed there are several connections that will need to be made, including power cabling and connections to the motherboard.

#### STEP 7 - Install the 2.5" Solid State Drives (SSD)

Next, it is time to install the solid state drives. Once the SSDs are seated, I simply used the screws in the mounts to secure the drives. Once the drives are installed there are several SATA connections to the motherboard that will need to be made as well as power cabling.

#### STEP 8 - Preparing for First Boot

Next, once all the components are installed I will take some time to clean-up the cabling and also double-check all connections have been made. One of the best features of the NZXT S340 case that I selected for my build is the multiple channels for hiding and securing cables. There is plenty of room to spare and it offers a very clean look as well as easy access.

#### STEP 9 - First Boot

Next, is the moment of triumph... time to switch on the power supply and push the power button. Success! I am greeted by the ASUS Bios menu and am able to verify all the installed components and devices. (e.g. Memory, Disk, etc)

#### STEP 10 - Wrap-Up the Build

Lastly, now that everything has checked out with the build it is time to install the operating systems, drivers, partition disks and etc.

---

## Next

Check out the next post for using a dual-boot configuration to run windows and linux.
