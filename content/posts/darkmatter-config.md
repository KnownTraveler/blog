---
date: "2018-04-21"
description: "Darkmatter Project Custom PC Dual-Boot Configuration"
title: "Darkmatter Project Custom PC Config"
slug: "darkmatter-config"
author:
  name: "Brian Hooper"
  email: "hooper@knowntraveler.io"
tags: [ "pc", "build", "darkmatter" ]
draft: false
summary: "Custom PC Build codenamed 'Darkmatter' dual-boot config for running Linux and Windows."
---

## TLDR

This post covers the install and dual boot configuration for my custom PC build codenamed, “Darkmatter”.

We will be installing Windows 10 and Linux Mint in a dual-boot configuration. The custom build uses three SSD Drives. The 250 GB M.2 NVMe Solid-State Drive that is mounted on the motherboard will be used to partition the Operating Systems whereas the additional storage available on the two 2.5” 250 GB SSDs will be used for windows and linux user/application directories.

---

## Resources

Here is a quick list of resources:

* [Rufus](https://rufus.ie/)
* [Etcher](https://www.balena.io/etcher/)
* [OS/Windows 10](https://www.microsoft.com/en-us/software-download/windows10ISO)
* [OS/Linux Mint](https://linuxmint.com/)
* [CAM by NZXT](https://www.nzxt.com/camapp)

---

## OS/Windows Installation

First, we will install the Windows OS on the new PC Build.

* Create a Bootable USB
* Install Windows from a USB
* Windows Setup and Install
* Windows Initial Boot
* Apply Windows Updates
* Rename the PC
* Disk Management

#### STEP 1 - Create bootable USB

* Use [Rufus](https://rufus.ie/) which is a utility that helps format and create bootable USB flash drives.

#### STEP 2 - Install Windows from USB

* Boot into UEFI BIOS Menu
* Select Bootable USB

#### STEP 3 - Windows Setup

* Select Languages and Etc
* Enter Product Key
* Accept Software License
* Select Drive
* Install the Operating Systems on the NVMe M.2 SSD that is mounted on the Motherboard.
* Windows Installs
* Windows Restarts

#### STEP 4 - Windows Initial Startup

* Cortana Assistant
* Select Region (United States)
* Select Keyboard Layout (US)
* Setup for Personal Use
* Sign In with Microsoft
* Create Pin
* Link Phone and PC
  * Do Later
* Link to One Drive
  * Only Save Files to this PC
* Make Cortana your Personal Assistant?
  * No
* Choose Privacy Settings
  * Disabled All
* May take several minutes to complete the install

#### STEP 5 - Apply Windows Updates

* Settings > Windows Updates
* Apply Updates
  * This can take awhile.
  * Grab some coffee, a beer or some bourbon.
  * Once complete, check that your system is up to date.

#### STEP 6 - Rename PC

* Pick a Kickass PC Name
* Restart

#### STEP 7 - Adjust Update Settings

Windows automatic updates are generally a good thing. But sometimes an update will make things worse. That’s why some people (including this guy) prefer to hold off on a update until other people have tried it without disaster.

Adjust your Update Settings to your liking.

#### STEP 8 - Disk Management

For my custom build I have three (3) SSD Drives intalled. Here is the basic strategy:

* Samsung 960 EVO NVMe M.2 SSD (250GB)
  * Use for Operating Systems (Linux/Windows)
* Samsung 860 EVO SSD (250GB)
  * Use for Windows
* Samsung 860 EVO SSD (250GB)
  * Use for Linux

Following the Windows Install, here is the partition scheme on initial boot:

**SAMSUNG 960 EVO (250GB SSD)**

* 500 MB NTFS OEM Partition
* 100 MB EFI System Partition
* 232 GB NTFS C:/ Partition with (179GB Free)

First, we need to shrink the C: Volume because basically we’re splitting the space in 1/2 with Windows C: using 116GB and 116GB Unallocoated for the Linux Install. Once we shrink the C: Volume next we will initialize the other Samsung 860 EVO (250 GB) drives.

**NOTES**:

* Use GPT Partition Style
* Verify Drive 1 and Drive 2 are Online and Available for Use.
* Drive 1 we’re going to use for Windows
  * Create a New Simple Volume
  * Format NTFS and Map to D:/
  * Label Volume (e.g. Pick a Name)
  * This is where we will install and keep our Windows Files for Smoother Recovery.
    * D:\Apps
    * D:\Downloads
* Examples:
  * Install Steam to here (D:\Apps\Steam)
  * Install NZXT CAM APP here (D:\Apps\NZXT\CAM)
  * Install Razer Synapse (D:\Apps\Razer)

---

## OS/Linux Installation

Now that we have Windows installed and our drives properly partitioned, we can proceed with installing Linux.

Here is a quick summary of the installation steps:

* Create a Bootable USB
* Install Linux Mint from a USB
* Linux Mint Setup
* Partition Scheme
* Complete Install and Reboot
* Configure Hardware Drivers
* Apply Package Updates
* Setup System Snapshots

#### STEP 1 - Create a Bootable USB

* Use [Etcher](https://www.balena.io/etcher/) which is a utility that helps format and create bootable USB flash drives.


#### STEP 2 - Install Linux from USB

* Boot into UEFI BIOS Menu (F2 Key)
* Select Bootable USB
* Note:
  * For my custom build, I’ll have to use regular monitor as NVIDIA Drivers aren’t installed by default
  * Click `e` to edit and replace “quiet spalsh” with “nomodeset”
  * [Read More](http://linuxmint-installation-guide.readthedocs.io/en/latest/boot_options.html)

#### STEP 3 - Linux Setup Menu

* Welcome
  * Select Language (English)
* Connect to the Internet
* Install Multimedia Codecs
  * Yes
* Choose an Installation Type
  * Because another operating system is present on the computer, the installer shows you an option to install Linux Mint alongside it
  * A boot menu is set up to choose between the two operating systems (i.e. Linux	Windows) each we start the PC.
  * Something Else (Create Partitions Yourself)
* Partition Scheme
  * Install root directory (i.e. / ) as new partition on OS Drive
  * Install swap directory (i.e. /swap ) as new parition on OS Drive
  * Install home directory (i.e. /home ) on 2nd Drive as a Separate Volume
  * Install Boot Record alongside Windows Boot Manager /dev/sda2
* Select Timezone
* Select Keyboard Layout
* Enter User Details
* Encrypt Home Folder (option)

#### STEP 4 - Linux Initial Startup

Following the Linux Mint install, one of the first thing to do is to check Hardware Drviers:

**Open “Driver Manager”**

* Select nvidia-384
* Select intel-microcode
* Click Apply

#### STEP 5 - Apply Updates

* Use `sudo apt-get update` command to fetch the available updates
* Use `sudo apt-get upgrade` command to upgrade the current packages

#### STEP 6 - Setup System Snapshots

* If anything goes wrong, you can restore your system from an earlier backup.
* Launch Menu > Administration > Timeshift
* Select a Drive and Scheduled Frequency

---

## Lastly

Ok, now on boot you should have the option to select which OS/(Linux|Windows) that you want to use.  

Personally I use Linux for work and Windows for PC gaming.
