---
date: "2018-01-01"
description: "ReadList"
title: "ReadList"
slug: "read-list"
author:
  name: "Brian Hooper"
  email: "hooper@knowntraveler.io"
tags: [ "readlist", "list" ]
draft: false
summary: "Currated list of articles, books, and posts worth reading."
---

## TLDR

This post is a currated list of articles, books, and posts I have compiled during my professional career that I believe are worth reading. They cover a range of topics from including Design, Engineering, Leadership, Technology, and History.

---

* [12 Rules for Life](https://www.jordanbpeterson.com/12-rules-for-life/) by Jordan Peterson
* [The Cathedral and the Bazaar](http://www.catb.org/~esr/writings/cathedral-bazaar/cathedral-bazaar/) by Eric Raymond
* [How to Become a Hacker](http://www.catb.org/~esr/faqs/hacker-howto.html) by Eric Raymond
* [Site Reliability Engineering](https://landing.google.com/sre/books/) by Google
* [Start with Why](https://simonsinek.com/product/start-with-why/) by Simon Sinek
* [Purple Cow](https://www.amazon.com/dp/B00316UMS0/) by Seth Godin
* [Drive](https://www.danpink.com/drive./) by Daniel Pink
* [REWORK](https://basecamp.com/books/rework) by Jason Fried, David Heinemeier Hansson
* [The Last Lecture](https://www.cmu.edu/randyslecture/) by Randy Pausch
