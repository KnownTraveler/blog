---
date: "2018-01-01"
description: "WatchList"
title: "WatchList"
slug: "watch-list"
author:
  name: "Brian Hooper"
  email: "hooper@knowntraveler.io"
tags: [ "watchlist", "list" ]
draft: false
summary: "Currated list of articles, books, and posts worth watching."
---

## TLDR

This post is a currated list of videos and presentations I have compiled during my professional career that I believe are worth watching. They cover a range of topics from including Design, Engineering, Leadership, Technology, and History.

---

### Creativity

* [Building Creative Confidence](https://www.ted.com/talks/david_kelley_how_to_build_your_creative_confidence) with David Kelley
* [Personalized Learning](https://www.youtube.com/watch?v=4Q7FTjhvZ7Y) with Sir Ken Robinson
* [School Kills Creativity](https://www.ted.com/talks/ken_robinson_says_schools_kill_creativity) with Sir Ken Robinson
* [Steve Jobs Philosophy](https://www.youtube.com/watch?v=zklbZR9025Y) with Steve Jobs

---

### Engineering

* [Are We There Yet?](https://www.infoq.com/presentations/Are-We-There-Yet-Rich-Hickey) with Rich Hickey
* [Back to the Future of 1994](https://www.ted.com/talks/danny_hillis_back_to_the_future_of_1994) with Danny Hillis
* [Site Reliability Engineering](https://landing.google.com/sre/books/) by Google
* [Simply Made Easy](https://www.infoq.com/presentations/Simple-Made-Easy) with Rich Hickey

---

### Golang

* [Why Golang is Successful](https://www.youtube.com/watch?v=cQ7STILAS0M) with Rob Pike
* [Concurrency is not Parallelism](https://www.youtube.com/watch?v=cN_DpYBzKso&t=4s) with Rob Pike
* [Advanced Testing with Go](https://www.youtube.com/watch?v=8hQG7QlcLBk) with Mitchell Hashimoto
* [Learn Go in 12 Minutes](https://www.youtube.com/watch?v=C8LgvuEBraI) with Jake Wright
* [7 Common Mistakes in Go, and when to avoid them](https://www.youtube.com/watch?v=29LLRKIL_TI&t=978s) with Steve Francia

---

### Happiness

* [Stumbling upon Happiness](https://www.ted.com/talks/dan_gilbert_asks_why_are_we_happy) with Dan Gilbert
* [The Happy Secret to Better Work](https://www.ted.com/talks/shawn_achor_the_happy_secret_to_better_work) with Shawn Achor
* [The Puzzle of Motivation](https://www.ted.com/talks/dan_pink_on_motivation) with Dan Pink

---

### JavaScript

* [JavaScript The Good Parts](https://www.youtube.com/watch?v=hQVTIJBZook) with Douglas Crockford
* [Crockford on JavaScript - Volume 1: The Early Years](https://www.youtube.com/watch?v=JxAXlJEmNMg&amp;list=PL7664379246A246CB&amp;index=1) with Douglas Crockford
* [Crockford on JavaScript - Chapter 2: And Then There Was JavaScript](https://www.youtube.com/watch?v=RO1Wnu-xKoY&amp;index=2&amp;list=PL7664379246A246CB) with Douglas Crockford
* [Crockford on JavaScript - Act III: Function the Ultimate](https://www.youtube.com/watch?v=ya4UHuXNygM&amp;index=3&amp;list=PL7664379246A246CB) with Douglas Crockford
* [Crockford on JavaScript - Episode IV: The Metamorphosis of Ajax](https://www.youtube.com/watch?v=Fv9qT9joc0M&amp;index=4&amp;list=PL7664379246A246CB) with Douglas Crockford
* [Crockford on JavaScript - Part 5: The End of All Things](https://www.youtube.com/watch?v=47Ceot8yqeI&amp;index=5&amp;list=PL7664379246A246CB) with Douglas Crockford

---

### Leadership

* [Start with Why](https://simonsinek.com/product/start-with-why/) by Simon Sinek
* [Purple Cow](https://www.amazon.com/dp/B00316UMS0/) by Seth Godin
* [Drive](https://www.danpink.com/drive./) by Daniel Pink
* [How Great Leaders Inspire Action](https://www.ted.com/talks/simon_sinek_how_great_leaders_inspire_action) with Simon Sinek
* [The Last Lecture](https://www.cmu.edu/randyslecture/) by Randy Pausch

---

### Open Source

* [12 Rules for Life](https://www.jordanbpeterson.com/12-rules-for-life/) by Jordan Peterson
* [The Cathedral and the Bazaar](http://www.catb.org/~esr/writings/cathedral-bazaar/cathedral-bazaar/) by Eric Raymond
* [How to Become a Hacker](http://www.catb.org/~esr/faqs/hacker-howto.html) by Eric Raymond
* [Netscape's Last Year and the rise of Mozilla](https://www.youtube.com/watch?v=4Q7FTjhvZ7Y0) a Documentary
* [Revolution OS](https://www.youtube.com/watch?v=jw8K460vx1c) a Documentary
* [The Mind Behind Linux](https://www.ted.com/talks/linus_torvalds_the_mind_behind_linux) with Linus Torvalds
* [Linus Torvalds on git](https://www.youtube.com/watch?v=4XpnKHJAok8) with Linus Torvalds

---

### Technology

* [Gate & Jobs at All Things Digital, 2007](https://www.youtube.com/watch?v=ZWaX1g_2SSQ) with Bill Gates & Steve Jobs

---

### Working

* [Building Businesses that Last](https://www.youtube.com/watch?v=of1zVJqZ-Cc) with Jason Fried
* [REWORK](https://basecamp.com/books/rework) by Jason Fried, David Heinemeier Hansson
* [Why Work Doesn't Happen at Work](https://www.youtube.com/watch?v=5XD2kNopsUs) with Jason Fried
